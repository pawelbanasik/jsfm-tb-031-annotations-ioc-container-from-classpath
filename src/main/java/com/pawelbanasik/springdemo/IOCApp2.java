package com.pawelbanasik.springdemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class IOCApp2 {

	public static void main(String[] args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(ConfigProperty.class);

		Organization org = (Organization) ctx.getBean("myorg");

		org.corporateSlogan();

		((AbstractApplicationContext) ctx).close();
	}
}
