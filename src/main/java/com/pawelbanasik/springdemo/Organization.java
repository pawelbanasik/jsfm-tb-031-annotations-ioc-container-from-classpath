package com.pawelbanasik.springdemo;

import org.springframework.stereotype.Component;

@Component("myorg")
public class Organization {

	public void corporateSlogan() {
		String slogan = "We build the ultimate driving machines!";
		System.out.println(slogan);

	}

}
